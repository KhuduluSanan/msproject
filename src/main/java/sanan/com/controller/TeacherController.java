package sanan.com.controller;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import sanan.com.model.Student;
import sanan.com.model.Teacher;
import sanan.com.service.TeacherService;

@RestController
@AllArgsConstructor
@RequestMapping("/teacher")
public class TeacherController {
    private ModelMapper modelMapper;
    private TeacherService teacherService;

    @GetMapping("/{id}")
    public Teacher getTeacherId(@PathVariable int id) {
        return teacherService.getTeacherId(id);
    }

    @PostMapping
    public String saveTeacher(@RequestBody Teacher teacher) {
        teacherService.saveTeacher(teacher);
        return "Added new person " + teacher.getName().toUpperCase();
    }

    @PutMapping("/{id}")
    public String updateTeacher(@RequestBody Teacher teacher) {
        teacherService.updateTeacher(teacher);
        return "Updated your info id =  " + teacher.getId();
    }

    @DeleteMapping
    public String deleteTeacher(@RequestParam(value = "id") int id) {
        teacherService.deleteTeacher(id);
        return "Deleted ";
    }
}
