package sanan.com.controller;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import sanan.com.model.Student;
import sanan.com.service.StudentService;

@RestController
@RequestMapping("/student")
@AllArgsConstructor
public class StudentController {
    // @Autowired bu anotation o de,ekdir ki bunun obyektini tap getir bura inject ele bura

    private ModelMapper modelMapper;
    private StudentService studentService;

    @GetMapping("{id}")
    public Student getStudent(@PathVariable int id) {
        return studentService.getStudentById(id);
    }

    @PostMapping   //Post sorqusunun bodysi olmalidir
    public String insertStudent(@RequestBody Student student) {
//      System.out.println(studentService); -------------------------
        studentService.saveStudet(student);
        return "Student name - " + student.getName();
    }

    @PutMapping("/{id}")
    public String uptadeStudent(@RequestBody Student student) {
       studentService.updateStudent(student);
       return "Deyisildi melumatlar";
    }

    @DeleteMapping
    public String deleteStudent(@RequestParam(value = "id") int id) {
        studentService.deleteStudent(id);
        return "Silindi";
    }


    /**   Path Variable ile olan versiya
     * @DeleteMapping("/{id}")
     * public String deleteId(@PathVariable(value = "id") int id) {
     *         studentService.deleteStudent(id);
     *         return "Silindi";
     *    }
     */


}
