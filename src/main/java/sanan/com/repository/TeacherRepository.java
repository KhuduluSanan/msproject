package sanan.com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sanan.com.model.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher,Integer> {
}
