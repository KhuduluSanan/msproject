package sanan.com.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sanan.com.model.Teacher;
import sanan.com.repository.TeacherRepository;


public interface TeacherService {
    Teacher getTeacherId(int id);
    void saveTeacher(Teacher teacher);
    void deleteTeacher(int id);
    void updateTeacher(Teacher teacher);
}
