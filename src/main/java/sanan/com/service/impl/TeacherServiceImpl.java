package sanan.com.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sanan.com.model.Teacher;
import sanan.com.repository.TeacherRepository;
import sanan.com.service.TeacherService;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private final TeacherRepository teacherRepository;

    @Override
    public Teacher getTeacherId(int id) {
        return teacherRepository.findById(id).get();
    }

    @Override
    public void saveTeacher(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    @Override
    public void deleteTeacher(int id) {
        teacherRepository.deleteById(id);
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        teacherRepository.save(teacher);
    }
}
