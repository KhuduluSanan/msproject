package sanan.com.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sanan.com.model.Student;
import sanan.com.repository.StudentRepository;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public Student getStudentById(int id) {
        return studentRepository.findById(id).get();
    }

    public void saveStudet(Student student) {
        studentRepository.save(student);
    }

    public void deleteStudent(int id) {
        studentRepository.deleteById(id);
    }

    public void updateStudent(Student student) {
        studentRepository.save(student);
    }
}
